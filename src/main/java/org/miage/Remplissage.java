package org.miage;

public class Remplissage {

    private static final String ESPACE = " ";

    private static boolean isEmpty(final CharSequence cs) {
        return cs.isEmpty();
    }

    /**
     * getChaine remplit à gauche une chaîne de caractères originale
     * avec une séquence de caractères sans dépasser la taille fournie 
     * en paramètre {@code taille}
     * 
     * @param chaineOriginale  la chaîne à compléter, peut être nulle
     * @param taille  la taille de la chaîne résultat
     * @param sequenceComplement  la séquence qui complète la chaîne originale. 
     * Une chaîne nulle ou vide est traitée comme un espace.
     * @return La méthode renvoie une chaîne de caractères remplie à gauche, 
     * la chaîne de caractères originale si aucun remplissage n'est nécessaire, 
     * ou {@code null} si une chaîne de caractères nulle est entrée.
     */
    public static String getChaine(final String chaineOriginale, final int taille, String sequenceComplement) {
        if (checkTailleSupZero(taille)) return chaineOriginale;

        // si chaine originale est null
        if (checkNullChaineOriginale(chaineOriginale)) return null;
        // si on ne veut compléter avec rien
        if (sequenceComplement == null || isEmpty(sequenceComplement)) {
            sequenceComplement = ESPACE;
        }

        // longueurSC = longueur du complement
        final int longueurSC = sequenceComplement.length();
        // longueurCO = longueur de chaine originale
        final int longueurCO = chaineOriginale.length();
        // nbCaracteresAInserer = nombre de caractères à insérer
        final int nbCaracteresAInserer = taille - longueurCO;

        // si il faut supprimer des caractères
        if (nbCaracteresAInserer < 0) {
            return chaineOriginale;
        }

        // Si il n'y a pas de modications à faire
        if (nbCaracteresAInserer == 0) {
            return chaineOriginale;
        // Si le complement est plus grand que le nombre de caractères a insérer
        } else if (nbCaracteresAInserer < longueurSC) {
            return sequenceComplement.substring(0, nbCaracteresAInserer).concat(chaineOriginale);
        // si le complement est plus petit que le nombre de caractères à insérer
        } else {
            // tableau de la taille du nombre de caractères à insérer
            final char[] complement = new char[nbCaracteresAInserer];
            // tableau des caractères à insérer
            final char[] caracteres = sequenceComplement.toCharArray();
            for (int i = 0; i < nbCaracteresAInserer; i++) {
                complement[i] = caracteres[i % longueurSC];
            }
            return new String(complement).concat(chaineOriginale);
        }
    }

    private static boolean checkTailleSupZero(int taille) {
        return taille <= 0;
    }

    private static boolean checkNullChaineOriginale(String chaineOriginale) {
        return chaineOriginale == null;
    }

}
