package org.miage;

import static org.miage.Remplissage.getChaine;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RemplissageTest {

    // quelques exemples:
    // "cool", 6, "-" -> "--cool"
    // "cool", 4, "-" -> "cool"
    // "cool", 6, "---" -> "--cool"
    // "", 6, "-" -> "------"

    private static final String CHAINE = "cool";

    @Test
    void pasDeModification() {
        assertThat(getChaine(CHAINE, 4, "-"))
                .isSameAs(CHAINE);
    }

    @Test
    void ajouter2caracteres() {
        assertThat(getChaine(CHAINE, 6, "-")).isEqualTo("--" + CHAINE);
    }

    @Test
    void ajouter2CaracteresPourTailleExacte() {
        assertThat(getChaine(CHAINE, 6, "--")).isEqualTo("--" + CHAINE);
    }

    @Test
    void ajouter3caracteresMaisTailleInf() {
        assertThat(getChaine(CHAINE, 6, "---")).isEqualTo("--" + CHAINE);
    }

    @Test
    void ajouter3caracteresTropDeCaractes() {
        assertThat(getChaine(CHAINE, 6, "abcdef")).isEqualTo("ab" + CHAINE);
    }

    @Test
    void ajouter3CaracteresSurChaineVide() {
        assertThat(getChaine("", 6, "-")).isEqualTo("------");
    }

    @Test
    void ajouterCaracteresSurNull() {
        assertThat(getChaine(null, 6, "---")).isNull();
    }

    @Test
    public void ajouterDifferentsCaracteres() {
        assertThat(getChaine(CHAINE, 6, "ab")).isEqualTo("ab" + CHAINE);
    }

    @Test
    public void ajouterDifferentsCaracteresAvecTailleNonComplete() {
        assertThat(getChaine(CHAINE, 7, "ab")).isEqualTo("aba" + CHAINE);
    }

    @Test
    public void ajouterCaracteresPourTailleInferieure() {
        assertThat(getChaine(CHAINE, -2, "-")).isSameAs(CHAINE);
    }

    @Test
    public void ajouterCaracteresPourTailleNulle() {
        assertThat(getChaine(CHAINE, 0, "-")).isSameAs(CHAINE);
    }

    @Test
    public void ajouterCaracteresVide() {
        assertThat(getChaine(CHAINE, 6, "")).isEqualTo("  " + CHAINE);
    }

    @Test
    public void ajouterCaracteresNull() {
        assertThat(getChaine(CHAINE, 6, null)).isEqualTo("  " + CHAINE);
    }


}
